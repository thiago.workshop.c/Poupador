package algoritmo;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class IA {
    int memoria[][];

    SensoresPoupador sensor;
    int contador = 0;

    public IA(SensoresPoupador sensorExterno, int[][] memoria) {

        this.sensor = sensorExterno;
        this.memoria = memoria;


    }


    //    public int explorador() {
//
//        memoria[sensor.getPosicao().x + 1][sensor.getPosicao().y + 1]++;
//        System.out.println("posição x: " + sensor.getPosicao().y + "posição y: " + sensor.getPosicao().x);
//        System.out.println(memoria[sensor.getPosicao().x][sensor.getPosicao().y]);
//
//
//        return proximoPasso();
//    }
    public int idle() {
        return 0;
    }

    public void searching_coins() {
        //TODO implementar procurar moedas
    }

    public void flee() {
        //TODO implementar fugir
    }

    public void go_to_bank() {
        //TODO implementar ir para o banco
    }

    public int[][] mapa() {

        //posicao atual
        Point pos_atual = sensor.getPosicao();

        //x e y do agente atual
        int x_agente = pos_atual.x;
        int y_agente = pos_atual.y;

        //definir as paredes do mapa
        //numero da parede: 100
        //numero da moeda: -100
        //numero do banco: 200
        int moeda = -1;
        int parede = 900;
        int banco = 200;

        for (int i = 0; i <= sensor.getVisaoIdentificacao().length; i++) {
            if (i <= 4) {
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                    memoria[y_agente - 2][(x_agente - 2) + i] = parede;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                    memoria[y_agente - 2][(x_agente - 2) + i] = moeda;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                    memoria[y_agente - 2][(x_agente - 2) + i] = banco;
                }

            }
            if (i >= 5 && i <= 9) {
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                    memoria[y_agente - 1][(x_agente - 2) + (i - 5)] = parede;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                    memoria[y_agente - 1][(x_agente - 2) + (i - 5)] = moeda;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                    memoria[y_agente - 1][(x_agente - 2) + (i - 5)] = banco;
                }
            }
            if (i >= 10 && i <= 13) {
                if (i <= 11) {
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                        memoria[y_agente][(x_agente - 2) + (i - 10)] = parede;
                    }
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                        memoria[y_agente][(x_agente - 2) + (i - 10)] = moeda;
                    }
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                        memoria[y_agente][(x_agente - 2) + (i - 10)] = banco;
                    }
                } else {
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                        memoria[y_agente][(x_agente - 2) + (i - 9)] = parede;
                    }
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                        memoria[y_agente][(x_agente - 2) + (i - 9)] = moeda;
                    }
                    if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                        memoria[y_agente][(x_agente - 2) + (i - 9)] = banco;
                    }
                }

            }
            if (i >= 14 && i <= 18) {
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                    memoria[y_agente + 1][(x_agente - 2) + (i - 14)] = parede;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                    memoria[y_agente + 1][(x_agente - 2) + (i - 14)] = moeda;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                    memoria[y_agente + 1][(x_agente - 2) + (i - 14)] = banco;
                }
            }
            if (i >= 19 && i <= 23) {
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 1) {
                    memoria[y_agente + 2][(x_agente - 2) + (i - 19)] = parede;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 4) {
                    memoria[y_agente + 2][(x_agente - 2) + (i - 19)] = moeda;
                }
                if (sensor.getVisaoIdentificacao()[i] != -1 && sensor.getVisaoIdentificacao()[i] == 3) {
                    memoria[y_agente + 2][(x_agente - 2) + (i - 19)] = banco;
                }
            }

        }
        return memoria;
    }

    public int explorador2() {

        ArrayList<PossivelComando> arrayPossiveisComandos = pegarPossiveisComandos();
        PesoSort sort = new PesoSort(arrayPossiveisComandos); //ordena o array recebido pela função pegarPossiveisComandos

        memoria[sensor.getPosicao().y][sensor.getPosicao().x]++; //incrementa onde o poupador passou na mente dele

        mapa();
        try {
            //verifica se a 3 pesos iguais e randomiza os 3
            if (arrayPossiveisComandos.get(0).getPeso() == arrayPossiveisComandos.get(1).getPeso() && arrayPossiveisComandos.get(0).getPeso() == arrayPossiveisComandos.get(2).getPeso()) {
                return arrayPossiveisComandos.get(new Random().nextInt(arrayPossiveisComandos.size())).getComando();
            }
        } catch (IndexOutOfBoundsException e) {
            return arrayPossiveisComandos.get(new Random().nextInt(arrayPossiveisComandos.size())).getComando();

        }
        //verifica se a 2 pesos iguais e randomiza os 2

        if (arrayPossiveisComandos.get(0).getPeso() == arrayPossiveisComandos.get(1).getPeso()) {
            return arrayPossiveisComandos.get(new Random().nextInt(arrayPossiveisComandos.size())).getComando();
        }
        //utlimo caso so tem tu vai tu mesmo.
        return arrayPossiveisComandos.get(0).getComando();

    }

    private void imprimirMemoria() {
        for (int i = 0; i < memoria.length; i++) {
            for (int j = 0; j < memoria.length; j++) {
                System.out.print("(" + memoria[i][j] + "),");
            }
            System.out.println(" ");
        }
    }


    //impressao de Array de comandos possiveis com Peso
    public void imprimirArray(ArrayList<PossivelComando> lista) {
        for (PossivelComando p : lista) {
            System.out.println("peso: " + p.getPeso() + " comando:" + p.getComando());

        }
    }


    //faz a checagem atravez da visao se o poupador esta ao lado de uma parede de forma com que apenas os comandos possiveis
    //sejam adicionado no array lista.
    public ArrayList<PossivelComando> pegarPossiveisComandos() {
        ArrayList<PossivelComando> lista = new ArrayList<>();
        //PAREDE
        if (sensor.getVisaoIdentificacao()[7] != 1 && sensor.getVisaoIdentificacao()[7] != -1 && sensor.getVisaoIdentificacao()[7] != 5) {
            lista.add(new PossivelComando(1, memoria[sensor.getPosicao().y - 1][sensor.getPosicao().x])); //cima
        }
        if (sensor.getVisaoIdentificacao()[11] != 1 && sensor.getVisaoIdentificacao()[11] != -1 && sensor.getVisaoIdentificacao()[11] != 5) {
            lista.add(new PossivelComando(4, memoria[sensor.getPosicao().y][sensor.getPosicao().x - 1])); //esquerda
        }
        if (sensor.getVisaoIdentificacao()[12] != 1 && sensor.getVisaoIdentificacao()[12] != -1 && sensor.getVisaoIdentificacao()[12] != 5) {

            lista.add(new PossivelComando(3, memoria[sensor.getPosicao().y][sensor.getPosicao().x + 1])); // direita
        }
        System.out.println(sensor.getVisaoIdentificacao()[16]);
        if (sensor.getVisaoIdentificacao()[16] != 1 && sensor.getVisaoIdentificacao()[16] != -1 && sensor.getVisaoIdentificacao()[16] != 5) {
            lista.add(new PossivelComando(2, memoria[sensor.getPosicao().y + 1][sensor.getPosicao().x])); //baixo
        }
        System.out.println("IMPRIMINDO O PESO DO PEGARPOSSIVEISCOMANDOS: ");
        imprimirArray(lista);
        System.out.println("posição atual y:" + sensor.getPosicao().y);
        System.out.println("posição atual x:" + sensor.getPosicao().x);
        imprimirMemoria();
        return lista;
    }


//    public int proximoPasso() {
//        int vetorDeMovimentos[] = new int[5];
//        try {
//
//
//            vetorDeMovimentos[3] = memoria[sensor.getPosicao().x + 2][sensor.getPosicao().y]; //direita
//            vetorDeMovimentos[4] = memoria[sensor.getPosicao().x - 2][sensor.getPosicao().y]; //esquerda
//            vetorDeMovimentos[1] = memoria[sensor.getPosicao().x][sensor.getPosicao().y - 2]; //cima
//            vetorDeMovimentos[2] = memoria[sensor.getPosicao().x][sensor.getPosicao().y + 2]; //baixo
//            System.out.println("direita " + vetorDeMovimentos[3]);
//            System.out.println("esquerda " + vetorDeMovimentos[4]);
//            System.out.println("cima " + vetorDeMovimentos[1]);
//            System.out.println("baixo " + vetorDeMovimentos[2]);
//
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("entrando aqui" + ((int) (Math.random() * 5)));
//            return (int) (Math.random() * 5);
//        }
//
//        return decicaoPorPeso(vetorDeMovimentos);
//    }

//    public int decicaoPorPeso(ArrayList<Integer> vetorDeMovimentos) {
//        int proximoProximoMov = 0;
//        int peso = 100;
//        for (int i = 0; i < vetorDeMovimentos.size(); i++) {
//            if (peso > vetorDeMovimentos[i]) {
//                proximoProximoMov = i;
//                peso = vetorDeMovimentos[i];
//
//
//            }
//
//        }
//        System.out.println("valor do peso atual" + peso);
//
//        System.out.println("resultado para switch " + proximoProximoMov);
//        switch (proximoProximoMov) {
//            case 3:
//                System.out.println("comando direita");
//                return 3;
//            case 4:
//                System.out.println("esquerda ");
//
//                return 4;
//            case 1:
//                System.out.println("comando cima");
//
//                return 1;
//            case 2:
//                System.out.println("comando baixo");
//
//                return 2;
//            default:
//                return ((int) (Math.random() * 4) + 1);
//        }
//    }

//    public void atualizaMemoria() {
//
//        if (sensor.getVisaoIdentificacao()[7] == 1 && sensor.getPosicao().y - 1 >= 0) {
//            memoria[sensor.getPosicao().x][sensor.getPosicao().y - 1] = 999; //cima
//        }
//        if (sensor.getVisaoIdentificacao()[11] == 1 && sensor.getPosicao().x - 1 >= 0) {
//            memoria[sensor.getPosicao().x - 1][sensor.getPosicao().y] = 999; //esquerda
//        }
//        if (sensor.getVisaoIdentificacao()[12] == 1 && sensor.getPosicao().x + 1 < 30) {
//            memoria[sensor.getPosicao().x + 1][sensor.getPosicao().y] = 999; //direita
//
//        }
//        if (sensor.getVisaoIdentificacao()[16] == 1 && sensor.getPosicao().y + 1 < 30) {
//            memoria[sensor.getPosicao().x][sensor.getPosicao().y + 1] = 999; //baixo
//
//        }
//    }
}
