package algoritmo;

public class PossivelComando {
    private int comando;
    private int peso;

    public void setComando(int comando) {
        this.comando = comando;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public PossivelComando(int comando, int peso) {
        this.comando = comando;
        this.peso = peso;
    }

    public int getComando() {
        return comando;
    }

    public int getPeso() {
        return peso;
    }
}
