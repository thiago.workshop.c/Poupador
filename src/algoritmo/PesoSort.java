package algoritmo;


import java.util.ArrayList;

public class PesoSort {


    //BUBBLE SORT para ordernar a lista atravez dos pesos
    public PesoSort(ArrayList<PossivelComando> lista) {
        for (int i = lista.size(); i >= 1; i--) {
            for (int j = 1; j < i; j++) {
                if (lista.get(j - 1).getPeso() > lista.get(j).getPeso()) {
                    int auxPeso = lista.get(j).getPeso();
                    int auxComando = lista.get(j).getComando();
                    lista.get(j).setPeso(lista.get(j-1).getPeso()); //v[j] = v[j - 1];
                    lista.get(j).setComando(lista.get(j-1).getComando()); //v[j] = v[j - 1];

                    lista.get(j-1).setPeso(auxPeso);
                    lista.get(j-1).setComando(auxComando);//v[j - 1] = aux;

                }
            }
        }


    }
}
